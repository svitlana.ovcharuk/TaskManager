package com.sveta.tmw.controller;

import com.sveta.tmw.entity.User;
import com.sveta.tmw.service.api.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserController {

  @Autowired
  UserServiceInterface userService;

  @GetMapping("/team/{taskId}")
  @ResponseStatus(HttpStatus.OK)
  List<User> getTeam(@PathVariable Integer taskId,
                     @RequestParam(name = "userId", required = false) int userId) throws SQLException {
    return userService.getTeamByTask(taskId, userId);
  }

  @GetMapping("/all")
  @ResponseStatus(HttpStatus.OK)
  List<User> getAll() throws SQLException {
    return userService.getAll();
  }

  @PostMapping("/add/")
  @ResponseStatus(HttpStatus.CREATED)
  User create(@RequestBody User user) {
    return userService.create(user);
  }

  @GetMapping("/id/{id}")
  @ResponseStatus(HttpStatus.OK)
  User get(@PathVariable Integer id) {
    return userService.findOne(id);
  }

  @GetMapping("/email/{email}")
  @ResponseStatus(HttpStatus.OK)
  User get(@PathVariable String email) {
    return userService.findByEmail(email);
  }

  @PutMapping("/update")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  boolean update(@RequestBody User user) {
    return userService.update(user);
  }

  @DeleteMapping("/del/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  boolean delete(@PathVariable Integer id) {
    return userService.delete(id);
  }


}