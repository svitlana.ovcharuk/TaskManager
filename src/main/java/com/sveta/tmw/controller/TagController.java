package com.sveta.tmw.controller;

import com.sveta.tmw.entity.Tag;
import com.sveta.tmw.service.impl.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/tags")
public class TagController {

  @Autowired
  private TagService tagService;

  @GetMapping
  List<Tag> getByProject(@RequestParam(name = "projectId", required = false) int projectId) {
    return tagService.getAllByProject(projectId);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Tag create(@RequestBody Tag tag) {
    return tagService.create(tag);
  }

  @GetMapping("/{id}")
  public Tag get(@PathVariable Integer id) {
    return tagService.findOne(id);
  }

  @PutMapping()
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public boolean update(@RequestBody Tag tag) {
    return tagService.update(tag);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public boolean delete(@PathVariable Integer id) {
    return tagService.delete(id);
  }


}
