package com.sveta.tmw.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sveta.tmw.entity.Status;
import com.sveta.tmw.service.api.EntityServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("api/status")
public class StatusController {

  EntityServiceInterface<Status> service;

  ObjectMapper objectMapper;

  @Autowired
  public StatusController(EntityServiceInterface<Status> service, ObjectMapper objectMapper) {
    this.service = service;
    this.objectMapper = objectMapper;
  }

  @GetMapping
  public List<Status> get() throws SQLException, JsonProcessingException {
    return service.getAll();

  }

  @PostMapping
  public Status create(@RequestBody Status status) throws SQLException {
    return service.create(status);
  }

  @DeleteMapping
  boolean delete(@RequestBody String json) throws SQLException, IOException {
    Status status = new Status();
    status = objectMapper.readValue(json, Status.class);
    return service.delete(status.getId());
  }

  @PutMapping
  boolean update(@RequestBody String json) throws SQLException, IOException {
    Status statusObj = objectMapper.readValue(json, Status.class);
    return service.update(statusObj);
  }

}