package com.sveta.tmw.dao.mapper;

import com.sveta.tmw.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {

  @Override
  public User mapRow(ResultSet rs, int i) throws SQLException {
    User u = new User();
    u.setId(rs.getInt("id"));
    u.setName(rs.getString("name"));
    u.setPass(rs.getString("pass"));
    u.setEmail(rs.getString("email"));
    u.setActivated(rs.getInt("active")>0);
    return u;
  }
}