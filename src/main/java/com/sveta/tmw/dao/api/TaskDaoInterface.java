
package com.sveta.tmw.dao.api;

import com.sveta.tmw.dao.util.JooqSQLBuilder;
import com.sveta.tmw.dto.TaskTableDTO;
import com.sveta.tmw.dto.TaskTreeDTO;
import com.sveta.tmw.entity.Comment;
import com.sveta.tmw.entity.Tag;
import com.sveta.tmw.entity.Task;

import java.util.List;

public interface TaskDaoInterface extends EntityDaoInterface<Task>{

    List<Tag> getTagsOfTask(int taskId);

    List<Comment> getCommentsOfTask(int taskId);

    List<Task> getSubtasks(int id);

    List<Task> getTasksAssignToUser(int userId);

    List<TaskTableDTO> getFilteredTasks(JooqSQLBuilder builder);

    List<Task> getPlannedTasks();

    List<TaskTreeDTO> findTaskByTree(int id, int userId);

    void refreshEstimateTimeOfParents(int id, int diffSpent, int diffLeft);

    List<Task> getParents(int id);

    List<Task> getLastChildrenProject(int projectId);

    List<Task> getLastChildrenTask(int id);

    boolean deleteTaskPlanning(int id);

}