package com.sveta.tmw.dao.api;

import com.sveta.tmw.entity.User;

public interface UserDaoInterface extends EntityDaoInterface<User> {

  User findByEmail(String email);
}
