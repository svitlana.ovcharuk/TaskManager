package com.sveta.tmw.dao.api;


import com.sveta.tmw.entity.User;
import com.sveta.tmw.entity.UsersTasks;

import java.util.List;

public interface UsersTasksDaoInterface extends EntityDaoInterface<UsersTasks>{

    public List<User> getTeamByTask(int taskId, int userId);

}
