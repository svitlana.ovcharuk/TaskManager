package com.sveta.tmw.dao.api;

import com.sveta.tmw.entity.Role;

import java.util.List;

public interface RoleDaoInterface extends EntityDaoInterface<Role> {

  List<Role> addBatch(Role... roles);

  boolean deleteAll();

}