package com.sveta.tmw.dao.api;

import com.sveta.tmw.entity.Tag;

import java.util.List;

public interface TagDaoInterface extends EntityDaoInterface<Tag> {

  List<Tag> getAllByProject(int projectId);

  List<Tag> getAllByTaskId(int taskId);

  boolean deleteAllByProject(int projectId);

  boolean deleteTagsOfTask(int taskId);

  boolean setTagsToTask(List<Tag> tags, int taskId);

}
