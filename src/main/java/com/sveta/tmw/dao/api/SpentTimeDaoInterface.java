package com.sveta.tmw.dao.api;

import com.sveta.tmw.entity.SpentTime;

public interface SpentTimeDaoInterface extends EntityDaoInterface<SpentTime>{

    public int getTotalSpentTimeByTask(int taskId);

}
