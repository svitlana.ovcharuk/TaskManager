package com.sveta.tmw.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Constraint(validatedBy = UniqUserEmailValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public  @interface UniqUserEmail {
	
	String message() default "{Not unique email}";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

}
