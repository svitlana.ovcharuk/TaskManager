package com.sveta.tmw.service.api;

import com.sveta.tmw.entity.Comment;

import java.util.List;

public interface CommentServiceInterface extends EntityServiceInterface<Comment>{

    boolean setCommentsToTask(List<Comment> commentList, int taskId);

    boolean deleteCommentsOfTask(int taskId);

    List<Comment> getCommentsByTaskId(int taskId);
}
