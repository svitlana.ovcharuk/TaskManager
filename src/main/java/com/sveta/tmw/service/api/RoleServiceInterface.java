package com.sveta.tmw.service.api;

import com.sveta.tmw.dao.api.EntityDaoInterface;
import com.sveta.tmw.entity.Role;

import java.util.List;

public interface RoleServiceInterface extends EntityDaoInterface<Role> {

  List<Role> addBatch(Role... roles);

  boolean deleteAll();
}
