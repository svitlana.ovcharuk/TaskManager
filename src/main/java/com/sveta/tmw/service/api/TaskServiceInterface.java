package com.sveta.tmw.service.api;

import com.sveta.tmw.dto.TaskDTO;
import com.sveta.tmw.dto.TaskFullInfoDTO;
import com.sveta.tmw.dto.TaskTableDTO;
import com.sveta.tmw.dto.TaskTreeDTO;
import com.sveta.tmw.entity.Comment;
import com.sveta.tmw.entity.SpentTime;
import com.sveta.tmw.entity.Tag;
import com.sveta.tmw.entity.Task;

import java.util.List;

public interface TaskServiceInterface extends EntityServiceInterface<Task> {

  List<Tag> getTagsOfTask(int taskId);

  List<Comment> getCommentsOfTask(int taskId);

  List<Task> getSubtasks(int id);

  List<Task> getTasksAssignToUser(int userId);

  List<TaskTableDTO> getFilteredTasksForTable(int parentId, String[] dates, int[] status,
                                              int[] priority, int[] tag, boolean planing, int userId);

  List<TaskTreeDTO> findTaskByTree(int id, int userId);

  TaskFullInfoDTO getFullInfo(int id);

  Task createTaskByDTO(TaskDTO taskDTO);

  boolean updateTaskByDTO(TaskDTO taskDTO);

  boolean updateCalendarTask(Task task);

  boolean deletePlanning(int id);

  List<Task> getPlannedTasks();

  boolean inviteUserToProject(String email, Integer projectId);

  boolean hasPermissionToUpdate(int userId, int taskId);

  boolean logTimeByTask(int userId, int taskId, int logTime, int spentTime);

  List<SpentTime> getWorkLogByTask(int taskId, int userId);

  int getTotalSpentTimeByTask(int taskId);
}