package it.com.sveta.tmw.dao;

import com.sveta.tmw.dao.api.UserDaoInterface;
import com.sveta.tmw.entity.User;
import it.com.sveta.tmw.configuration.TestConfig;
import it.com.sveta.tmw.dao.utility.UserPopulator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserDaoItTest {

  @Autowired
  private UserDaoInterface userDao;
  @Autowired
  private UserPopulator populator;
  @Autowired
  private PasswordEncoder passwordEncoder;

  @Test
  public void iTshouldInsertAndGetOneAndDelete() throws SQLException {
    // Given
    User userNew = new User();
    userNew.setName("if-078");
    userNew.setEmail("softServeAcademy@gmail.test");
    userNew.setPass(passwordEncoder.encode("1111111"));
    User userFindOne;
    // When
    userNew = userDao.create(userNew);
    userFindOne = userDao.findOne(userNew.getId());
    // Then
    assertThat(userNew.getEmail()).isEqualTo(userFindOne.getEmail());
    assertThat(userDao.delete(userFindOne.getId())).isTrue();
  }

  @Test
  public void iTshouldInsertAndGetByEmailAndGetAll() throws SQLException {
    // Given
    User userByEmail;
    User userNew = populator.createCustomUser("Academy", "soft@serve.com");
    // When
    userByEmail = userDao.findByEmail(userNew.getEmail());
    // Then
    assertThat(userNew.getEmail()).isEqualTo(userByEmail.getEmail());
    assertEquals(true, userDao.getAll().size() >= 1);
  }
}